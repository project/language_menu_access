<?php

/**
 * @file
 * Allows showing/hiding menu items per language.
 */

/**
 * Implements hook_init().
 */
function language_menu_access_init() {
  module_load_include('admin.inc', 'language_menu_access');
}

/**
 * Implements hook_permission().
 */
function language_menu_access_permission() {
  return array(
    'administer menus per language' => array(
      'title' => t('Administer menus per language'),
    ),
  );
}

/**
 * Implements hook_translated_menu_link_alter().
 */
function language_menu_access_translated_menu_link_alter(&$item, $map) {
  global $language;

  // Do not hide menu item when we are editing either a specific menu item,
  // or a menu it belongs to, to avoid setting it to hidden by accident.
  // The challenge here is that we want to keep original $item['hidden']
  // value only in the menu item edit form, while still use value altered
  // by this hook when menu item is displayed outside of edit form.
  // @TODO: Rethink?
  // I still don't quite like this check, although it's already better
  // (again) compared to its previous versions.
  if (
    $_GET['q'] == 'admin/structure/menu/manage/' . $item['menu_name']
    || $_GET['q'] == "admin/structure/menu/item/{$item['mlid']}/edit"
  ) {
    foreach (debug_backtrace() as $trace) {
      if (in_array($trace['function'], array('menu_overview_form', 'menu_get_item'))) {
        return;
      }
    }
  }

  // Process menu items to be shown per domain.
  if (isset($item['options']['language_menu_access']['show']) && count($item['options']['language_menu_access']['show'])) {
    $item['hidden'] = (isset($item['options']['language_menu_access']['show'][$language->language]) && $item['options']['language_menu_access']['show'][$language->language]) ? 0 : 1;
  }
  // Process menu items to be hidden per domain.
  if (isset($item['options']['language_menu_access']['hide'][$language->language])) {
    $item['hidden'] = ($item['options']['language_menu_access']['hide'][$language->language]) ? 1 : 0;
  }
}
